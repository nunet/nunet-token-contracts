let NuNetToken = artifacts.require("./NuNetToken.sol");

const name = "NuNet Utility Token"
const symbol = "NTX"

module.exports = function (deployer) {
    deployer.deploy(NuNetToken, name, symbol);
  };

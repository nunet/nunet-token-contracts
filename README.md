# nunet-token-contracts
Includes token contracts, migrations, tests

### NuNetToken
* ERC-20 implementation for NuNet NTX Token

## Deployed Contracts
* NuNetToken (Mainnet): [0xf0d33beda4d734c72684b5f9abbebf715d0a7935](https://etherscan.io/address/0xf0d33beda4d734c72684b5f9abbebf715d0a7935)
* NuNetToken (Ropsten) : [0x7F44Bc93BCb96011800Da80d1a31E05e5A5AC7f9](https://ropsten.etherscan.io/address/0x7F44Bc93BCb96011800Da80d1a31E05e5A5AC7f9)
* NuNetToken (Goerli) : [0x62221B6BcF322b7E77D8Ab5Cd46a01DB8a263741](https://goerli.etherscan.io/address/0x62221B6BcF322b7E77D8Ab5Cd46a01DB8a263741)

## Requirements
* [Node.js](https://github.com/nodejs/node) (12+)
* [Npm](https://www.npmjs.com/package/npm)

## Install

### Dependencies
```bash
npm install
```

### Test 
```bash
npm run test
```

## Package
```bash
npm run package-npm
```

## Release
NuNetToken artifacts are published to NPM: https://www.npmjs.com/package/nunet-token-contracts
=======
ERC-20 implementation for NuNet NTX Token

